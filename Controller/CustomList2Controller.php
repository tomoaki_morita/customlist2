<?php

/*
 * This file is part of the CustomList2
 *
 * Copyright (C) 2018 rinfinity
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CustomList2\Controller;

use Eccube\Application;
use Symfony\Component\HttpFoundation\Request;

class CustomList2Controller
{

    /**
     * CustomList2画面
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request)
    {

        // add code...

        return $app->render('CustomList2/Resource/template/index.twig', array(
            // add parameter...
        ));
    }

}
