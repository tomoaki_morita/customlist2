<?php

/*
 * This file is part of the CustomList2
 *
 * Copyright (C) 2018 rinfinity
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CustomList2;

use Eccube\Application;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class CustomList2Event
{

    /** @var  \Eccube\Application $app */
    private $app;

    /**
     * CustomList2Event constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRouteAdminContentRequest(GetResponseEvent $event)
    {
    }

}
