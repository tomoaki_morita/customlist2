<?php

/*
 * This file is part of the CustomList2
 *
 * Copyright (C) 2018 rinfinity
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\CustomList2\ServiceProvider;

use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Plugin\CustomList2\Form\Type\CustomList2ConfigType;
use Silex\Application as BaseApplication;
use Silex\ServiceProviderInterface;

class CustomList2ServiceProvider implements ServiceProviderInterface
{

    public function register(BaseApplication $app)
    {   
        

        // リスト用リポジトリ
        $app['eccube.plugin.custom_list2.repository.custom_list2'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Plugin\CustomList2\Entity\CustomList2');
        });

        // プラグイン用設定画面
        $app
            ->match('/'.$app['config']['admin_route'].'/plugin/CustomList2/config', 'Plugin\CustomList2\Controller\ConfigController::index'
            )->bind('plugin_CustomList2_config');

        // 独自コントローラ
        $app->match('/plugin/customlist2/hello', 'Plugin\CustomList2\Controller\CustomList2Controller::index')->bind('plugin_CustomList2_hello');

        // Form
        $app['form.types'] = $app->share($app->extend('form.types', function ($types) use ($app) {
            $types[] = new CustomList2ConfigType();

            return $types;
        }));

        // 管理画面定義
        $admin = $app['controllers_factory'];
        // 強制SSL
        if ($app['config']['force_ssl'] == Constant::ENABLED) {
            $admin->requireHttps();
        }

        // リスト一覧
        $admin->match('/plugin/custom_list2/lists', '\\Plugin\\CustomList2\\Controller\\CustomList2Controller::index')
            ->value('id', null)->assert('id', '\d+|')
            ->bind('plugin_custom_list2');

        // リスト編集
        $admin->match('/plugin/custom_list2/template/{id}/edit', '\\Plugin\\CustomList2\\Controller\\CustomList2Controller::edit')
            ->value('id', null)->assert('id', '\d+|')
            ->bind('plugin_custom_list2_edit');

        // リスト登録
        $admin->match('/plugin/custom_list2/lists/regist', '\\Plugin\\CustomList2\\Controller\\CustomList2Controller::regist')
            ->bind('plugin_custom_list2_regist');

        // リスト編集確定
        $admin->match('/plugin/custom_list2/lists/commit/{id}', '\\Plugin\\CustomList2\\Controller\\CustomList2Controller::commit')
            ->value('id', null)->assert('id', '\d+|')
            ->bind('plugin_custom_list2_commit');

        // リスト削除
        $admin->match('/plugin/custom_list2/lists/{id}/delete', '\\Plugin\\CustomList2\\Controller\\CustomList2Controller::delete')
            ->value('id', null)->assert('id', '\d+|')
            ->bind('plugin_custom_list2_delete');


        $app['config'] = $app->share(
            $app->extend(
                'config',
                function ($config) {
                    $addNavi['id'] = 'plugin_custom_list2';
                    $addNavi['name'] = 'カスタムリスト登録';
                    $addNavi['url'] = 'plugin_custom_list2';
                    $nav = $config['nav'];
                    foreach ($nav as $key => $val) {
                        if ('setting' == $val['id']) {
                            array_splice($nav, $key, 0, array($addNavi));
                            break;
                        }
                    }
                    $config['nav'] = $nav;

                    return $config;
                }
            )
        );
        

        // ログファイル設定
        $app['monolog.logger.customlist2'] = $app->share(function ($app) {

            $logger = new $app['monolog.logger.class']('customlist2');

            $filename = $app['config']['root_dir'].'/app/log/customlist2.log';
            $RotateHandler = new RotatingFileHandler($filename, $app['config']['log']['max_files'], Logger::INFO);
            $RotateHandler->setFilenameFormat(
                'customlist2_{date}',
                'Y-m-d'
            );

            $logger->pushHandler(
                new FingersCrossedHandler(
                    $RotateHandler,
                    new ErrorLevelActivationStrategy(Logger::ERROR),
                    0,
                    true,
                    true,
                    Logger::INFO
                )
            );

            return $logger;
        });

    }

    public function boot(BaseApplication $app)
    {
    }

}
