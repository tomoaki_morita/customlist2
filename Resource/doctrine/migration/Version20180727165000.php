<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version201508072300 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->createCustomList2($schema);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('plg_custom_list2');
    }

    /***
     * plg_mailmaga_templateテーブルの作成
     * @param Schema $schema
     */
    protected function createCustomList2(Schema $schema)
    {
        $table = $schema->createTable('plg_custom_list2');
        $table->addColumn('custom_list_id', 'integer', array(
            'autoincrement' => true,
        ));

        $table->addColumn('subject', 'text', array(
        ));

        $table->addColumn('body', 'text', array(
        ));

        $table->addColumn('del_flg', 'smallint', array(
            'notnull' => true,
            'unsigned' => false,
            'default' => 0,
        ));

        $table->addColumn('create_date', 'datetime', array(
            'notnull' => true,
            'unsigned' => false,
        ));

        $table->addColumn('update_date', 'datetime', array(
            'notnull' => true,
            'unsigned' => false,
        ));

        $table->setPrimaryKey(array('custom_list_id'));
    }

}
